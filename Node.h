#ifndef NODE_H
#define NODE_H

#include "Graph.h"
#include <vector>
#include <string>

using namespace std;

class Node{
public:
	string label;
	vector<pair<Node*, int> > arcs;

    Node(string);
    Node();
    virtual ~Node();

    void print_arcs();

};

#endif // NODE_H
