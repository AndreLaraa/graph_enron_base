#include <iostream>
#include <list>
#include "Graph.h"

using namespace std;

Node::Node(string r){
    this->label = r;
}
Node::Node(){}
Node::~Node(){}

void Node::print_arcs() {
    cout << label << endl;
    for (unsigned int i = 0; i < arcs.size();i++)
        cout << "\t" << arcs[i].first->label << "\t Peso: " << arcs[i].second << endl;
}
