#include "Graph.h"
#include <iterator>
#include <algorithm>    // std::find
#include <climits>

using namespace std;

Graph::Graph(){}
Graph::~Graph(){}

void Graph::print_all_arcs(){
    
    for (unsigned int i = 0; i < nodes.size();i++)
        nodes[i]->print_arcs();
    cout << endl;
}

void Graph::insert_node(Node* i) {
    nodes.push_back(i);
}

void Graph::define_arc(Node *from, Node *to, int w) {
    from->arcs.push_back(pair<Node*, int>(to, w));
}

void Graph::remove_arc(Node *from, Node *to) {
    for (unsigned int k = 0; k < from->arcs.size();k++) {
        if (from->arcs[k].first == to)
            from->arcs.erase(from->arcs.begin() + k);
    }
}

void Graph::set_info(Node *in, string label) {
    in->label = label;
}

int Graph::arcs(Node *in, vector<pair<Node*, int> > &vector_arcs) {
    for (unsigned int i = 0; i < in->arcs.size();i++)
        vector_arcs.push_back(in->arcs[i]);
    
    return vector_arcs.size();
}

void Graph::dijkstra(Node* from, Node* to){
	fill_MatrixAdj(); 
    int size = nodes.size();
	int* distance = new int[size];
	int* prev = new int[size];
    bool* visited = new bool[size];
	
	
    for (int i = 0; i < size ; i++){
        distance[i] = INT_MAX;
        visited[i] = false;
        prev[i] =-1;
    }

	if (from){
		int indice = indexOf(from);
		cout << "Distances from " << indice << endl;
		distance[indice] = 0;
	} else {
		cout << "Distances from 0" << endl;
		distance[0] = 0;
	}
    
    for(int i = 0 ; i < size - 1; i++){
       
        int minVertex = findMinVertex(distance, visited, size);
        visited[minVertex] = true;
        for(int j = 0; j < size; j++){
            if (matrixadj[minVertex][j] != 0 && !visited[j]){
                int dist = distance[minVertex] + matrixadj[minVertex][j];
                if (dist < distance[j]){
                    distance[j] = dist;
                    prev[j] = minVertex;
                }
            }
        }
    }

    for(int i = 0; i < size; i++){
        cout << "Para " << i << " Custo: " << distance[i];
        imprimeCaminho(indexOf(from), i, prev);
    }
    
    if (to){
		cout <<"The distance between " << from->label << " and " << to->label << " is " << distance[indexOf(to)] << endl;
		imprimeCaminho(indexOf(from), indexOf(to), prev);
	}
	
    cout << endl;
    delete [] visited;
    delete [] distance;
}
void Graph::imprimeCaminho(int origem, int destino, int*caminho){
	int i,j = 0;
	int *inverse = new int[nodes.size()];
	
	if (caminho[destino] == -1) {
		cout << endl <<  "Não há caminho entre " << origem << "(" << nodes[origem]->label <<  ") e " << destino << "(" << nodes[destino]->label << ")" << endl;
		return;
	}
	
	i = caminho[destino];
	//cout << endl << "[" << destino ;
	int count = 0;
	inverse[count++] = destino;
	
	while (i != origem){
		//cout << " - " << i;
		inverse[count++] = i;
		i = caminho[i];		
	}
	// << " - " << i << "]" << endl << endl;
	
	inverse[count] = i;
	cout << endl << "[" << inverse[count--];
	
	while (count != 0){
		cout << " - " << inverse[count--];
	}
	cout << " - " << inverse[count] << "]" << endl << endl;		
	 	
}

int Graph::findMinVertex(int* distance, bool* visited, int n){
    int minVertex = -1;
    for(int i = 0; i < n; i++){
        if(!visited[i] && (minVertex == -1 || distance[i] < distance[minVertex])){
            minVertex = i;
        }
    }
    return minVertex;
}

int Graph::indexOf(Node* node){
	ptrdiff_t pos = distance(nodes.begin(), find(nodes.begin(), nodes.end(), node));
	return pos;
}

void Graph::fill_MatrixAdj(){
	int size = nodes.size();
    matrixadj = new double*[size];

    //Preenche a matriz de adjacencia com 0 (deve ser infinito)
    for(int i = 0; i < size; i++){
        matrixadj[i] = new double[size];
        for(int j = 0; j < size; j++){
            matrixadj[i][j] = 0;
        }
    }

    //Preenche matrixAdj com os pesos
    for (unsigned int i = 0; i < size; i++){ //percorre lista principal
        for (unsigned int k = 0; k < nodes[i]->arcs.size();k++) { //percorre adjacentes de cada item da lista principal
        	int indexAdj = indexOf(nodes[i]->arcs[k].first); //index do adjacente dentro da lista principal
        	matrixadj[i][indexAdj] = nodes[i]->arcs[k].second;
    	}
    }
}

void Graph::print_matrixAdj(){
	int size = nodes.size();
	for(int i = 0; i < size; i++){
    	for(int j = 0; j < size; j++){
        	cout << this->matrixadj[i][j];
        	if (j < size - 1) cout << ", ";
        }
    	cout << endl;
    }
}
