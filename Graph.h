#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include "Node.h"
#include <iterator>
#include <string>

using namespace std;

class Graph
{

public:
	vector<Node*> nodes;
	double** matrixadj;

    Graph();
    virtual ~Graph();

    void print_all_arcs();
    void insert_node(Node*);
    void define_arc(Node *, Node *, int);
    void remove_arc(Node *, Node *);
    void set_info(Node*, string);
    int arcs(Node *, vector<pair<Node*, int> > &);
    
	void dijkstra(Node* from = NULL, Node* to = NULL);
    int indexOf(Node*);
	void fill_MatrixAdj();
    void print_matrixAdj();
    int findMinVertex(int*, bool*, int);
    void imprimeCaminho(int ,int ,int*);
};

#endif // GRAPH_H
