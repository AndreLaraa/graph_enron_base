#include <iostream>
#include <fstream>
#include <vector>
#include "Graph.h"
#include <dirent.h>
#include <sys/stat.h>
#include <regex>
#include <string>

using namespace std;

vector<string> split(const char *str, char c = ' '){
    vector<string> result;
    do{
        const char *begin = str;
        while(*str != c && *str)
            str++;
        result.push_back(string(begin, str));
    } while (0 != *str++);
    return result;
}

void readEmail(string pathFile, bool firstEmail, Graph *G, string *from){
    ifstream emailFile;
    emailFile.open(pathFile);
    if (emailFile.fail()){
        cout << "Fail" << endl;
        return ;
    }

    //MARK: - Line by line
    string line = "";
    unsigned int foundLine = 0;
    bool verify = false;

    vector<string> tokensLine;
    vector<string> emailOfFile;
    const char * charToToken;

    do{
        getline(emailFile, line);

        if (firstEmail){
            foundLine = line.find_first_not_of("From:",0,5);
            if (foundLine) {
                charToToken = line.c_str();
                tokensLine = split(charToToken, ' ');
                *from = tokensLine[1];
                G->insert_node(*from);
                firstEmail = false;
            }
        }

        foundLine = line.find_first_not_of("Subject:",0,8);
        if (foundLine) {
            if (emailOfFile.size() > 0)
                emailOfFile.erase(emailOfFile.begin());
            break;
        }

        foundLine = line.find_first_not_of("To:",0,3);
        if (foundLine || verify) {
            //Token the Line
              charToToken = line.c_str();
              tokensLine = split(charToToken, ' ');

            for (int i = 0; i < tokensLine.size(); i++) {

                if (!tokensLine[i].find_first_not_of(" :",0)){
                    string regexString = regex_replace(tokensLine[i],regex("\\s+"), "");
                    regexString = regex_replace(regexString,regex(","), "");
                    emailOfFile.push_back(string(regexString));
                }
            }
            verify = true;
        }

    }while(!emailFile.eof());
    emailFile.close();

    string to;
    if (emailOfFile.size()) {
        for (int c = 0; c < emailOfFile.size(); c++) {
            to = string(emailOfFile[c]);
            G->cria_adjacencia(*from, to);
        }
    }

}

int exploreEachEmail(char *dir_name, Graph *G){
    DIR *dir; //pointer to an open director
    struct dirent *entry; //stuff in the directory
    struct stat info;   //information about each entry
    bool firstMailOfThePerson = true;

    dir = opendir(dir_name);

    if(!dir){
        cout << "Directory " << string(dir_name) << " was not found\n" << endl;
        return 0;
    }
    string from;

    while ( (entry = readdir(dir))  != NULL){

        if( entry->d_name[0] != '.'){
            string path = string(dir_name) + "\\" + string(entry->d_name);

            if (!S_ISDIR(info.st_mode)){

                readEmail(path, firstMailOfThePerson, G, &from);
                firstMailOfThePerson = false;
            }
        }
    }
    closedir(dir);
    return 1;
}

vector<string> searchPeople(char *dir_name){
    vector<string> people;
    DIR *dir; //pointer to an open director
    struct dirent *entry; //stuff in the directory

    dir = opendir(dir_name);
    if(!dir){
        cout << "Directory "<< string(dir_name) <<" was not found\n" << endl;
        return people;
    }

    while ( (entry = readdir(dir))  != NULL)
        if( entry->d_name[0] != '.')
            people.push_back(string(entry->d_name));

    closedir(dir);
    return people;
}

int main() {

    Graph *G = new Graph();

    string raiz = "D:\\untitled\\maildir\\";
    string path = raiz;
    vector<string> pessoas = searchPeople((char*)path.c_str());

    int sent_mail = 0;
    for (int i = 0 ; i < pessoas.size();i++){
        path = string(raiz + pessoas[i] + "\\sent");
        sent_mail = exploreEachEmail((char*)path.c_str(), G);
        if (sent_mail == 0 ){
            path = string(raiz + pessoas[i] + "\\_sent_mail");
            exploreEachEmail((char*)path.c_str(), G);
        }

    }
    cout << G->nodes.size();
    cout << " " << G->count_edges();

    return 0;
}

